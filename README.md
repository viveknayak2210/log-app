# Log App

This is a logger app for the voting dapp ('https://gitlab.com/viveknayak2210/voting-dapp.git')

### Version
1.1.0

### Usage
Loginapp requires [Node.js](https://nodejs.org/) v4+ to run.
1. Install mongoDB at port 12017

2. Pull code, cd to this folder and install all dependencies
```sh
$ npm install
```

5. Start server
```sh
$ node app
```

6. Go to http://localhost:4000/