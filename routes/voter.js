var express = require('express');
var router = express.Router();
var alert;
var Voter = require('../models/voter');
var sha = require('sha256');
router.get('/', function (req, res) {
	res.render('voter');
});

router.post('/', function (req, res) {
	var id = req.body.id;
	var token = sha(id);
	var newVoter = new Voter({
						hash:sha(token)
					});
	Voter.createVoter(newVoter, function (err, voter) {
						if (err) throw err;
						console.log(voter);
						req.flash('success_msg','Your token is: '+token);
						res.redirect('/voter');
	});
});

module.exports = router;