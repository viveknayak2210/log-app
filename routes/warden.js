var express = require('express');
var router = express.Router();

var Warden = require('../models/warden');

router.get('/', function (req, res) {
	res.render('warden');
});

router.post('/', function (req, res) {
	var address = req.body.address;
	var newWarden = new Warden({
		address: address
	});
	Warden.createWarden(newWarden, function (err, warden) {
		if (err) throw err;
		console.log(warden);
		res.redirect('/warden');
	});

});

module.exports = router;