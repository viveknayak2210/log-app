var express = require('express');
var router = express.Router();

var Candidate = require('../models/candidate');

router.get('/', function (req, res) {
	res.render('candidate');
});

router.post('/', function (req, res) {
	var name = req.body.name;
	var newCandidate = new Candidate({
		name: name
	});
	Candidate.createCandidate(newCandidate, function (err, candidate) {
		if (err) throw err;
		console.log(candidate);
		res.redirect('/candidate');
	});

});

module.exports = router;