var mongoose = require('mongoose');
// Warden Schema
var WardenSchema = mongoose.Schema({
	address: {
		type: String,
		index:true
	}
});

var Warden = module.exports = mongoose.model('Warden', WardenSchema);

module.exports.createWarden = function(newWarden, callback){
	newWarden.save(callback);
}

module.exports.getWardenDatabase= function(callback){
	Warden.find({},callback);
}